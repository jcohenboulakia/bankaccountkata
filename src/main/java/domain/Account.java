package domain;

import domain.exception.NotEnoughMoneyException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Account {
    private BigDecimal balance;

    private List<Operation> operations;

    public Account() {
        this.balance = new BigDecimal(0);
        this.operations = new ArrayList<>();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void addOperation(Operation operation) {
        switch (operation.getOperationType()) {
            case DEPOSIT:
                balance = balance.add(operation.getAmount());
                break;
            case WITHDRAWAL:
                if (balance.compareTo(operation.getAmount()) < 0){
                    throw new NotEnoughMoneyException();
                }
                balance = balance.subtract(operation.getAmount());
                break;
        }
        operations.add(operation);
    }

    public String getHistory() {
        StringBuilder historyStringBuilder = new StringBuilder();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        operations.forEach(operation -> {
            historyStringBuilder.append(dateFormat.format(operation.getDate()));
            historyStringBuilder.append(" - ");
            historyStringBuilder.append(operation.getOperationType());
            historyStringBuilder.append(" - ");
            historyStringBuilder.append(operation.getAmount());
            historyStringBuilder.append("\n");

        });
        historyStringBuilder.append("Balance : "+ getBalance());
        return historyStringBuilder.toString();
    }
}
