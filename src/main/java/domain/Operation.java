package domain;

import domain.exception.IncorrectOperationException;

import java.math.BigDecimal;
import java.util.Date;

public class Operation {
    private final Date date;
    private final BigDecimal amount;
    private final OperationType operationType;

    public Operation(Date date, BigDecimal amount, OperationType operationType) {
        if (amount.compareTo(new BigDecimal(0)) < 1) {
            throw new IncorrectOperationException();
        }
        this.date = date;
        this.amount = amount;
        this.operationType = operationType;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operation that = (Operation) o;

        if (getDate() != null ? !getDate().equals(that.getDate()) : that.getDate() != null) return false;
        if (getAmount() != null ? !getAmount().equals(that.getAmount()) : that.getAmount() != null) return false;
        return getOperationType() == that.getOperationType();

    }

    @Override
    public int hashCode() {
        int result = getDate() != null ? getDate().hashCode() : 0;
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getOperationType() != null ? getOperationType().hashCode() : 0);
        return result;
    }
}
