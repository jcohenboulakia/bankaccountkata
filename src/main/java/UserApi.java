import domain.Account;
import domain.Operation;
import domain.OperationType;
import infra.CurrentDate;

import java.math.BigDecimal;

public class UserApi {

    private CurrentDate currentDate;

    public UserApi(CurrentDate currentDate) {
        this.currentDate = currentDate;
    }


    public void deposit(Account account, BigDecimal amount) {
        Operation operation = new Operation(currentDate.getCurrentDate(), amount, OperationType.DEPOSIT);
        account.addOperation(operation);
    }

    public void withdrawal(Account account, BigDecimal amount) {
        Operation operation = new Operation(currentDate.getCurrentDate(), amount, OperationType.WITHDRAWAL);
        account.addOperation(operation);
    }

    public String history(Account account) {
        return account.getHistory();
    }


}
