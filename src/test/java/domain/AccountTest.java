package domain;

import domain.Account;
import domain.Operation;
import domain.OperationType;
import domain.exception.NotEnoughMoneyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class AccountTest {

    @Test
    void constructor_should_set_balance_0_and_initialize_list_of_operation() {
        Account account = new Account();
        Assertions.assertEquals(new BigDecimal(0), account.getBalance());
        Assertions.assertEquals(new ArrayList<Operation>(), account.getOperations());

    }

    @Test
    public void deposit_should_add_amount_to_account_and_store_operation() {
        //Given
        Operation operation = new Operation(new Date(1563366782), new BigDecimal(100), OperationType.DEPOSIT);
        Account account = new Account();
        List<Operation> expectedOperationsList = new ArrayList<>();
        expectedOperationsList.add(operation);

        //When
        account.addOperation(operation);

        //Then
        Assertions.assertEquals(new BigDecimal(100), account.getBalance());
        Assertions.assertEquals(expectedOperationsList, account.getOperations());
    }

    @Test
    public void withdrawal_should_take_off_amount_from_account_and_store_operation() {
        //Given
        Operation deposit = new Operation(new Date(1563366782), new BigDecimal(100), OperationType.DEPOSIT);
        Operation withdrawal = new Operation(new Date(1563366800), new BigDecimal(20), OperationType.WITHDRAWAL);
        Account account = new Account();
        List<Operation> expectedOperationsList = new ArrayList<>();
        expectedOperationsList.add(deposit);
        expectedOperationsList.add(withdrawal);

        //When
        account.addOperation(deposit);
        account.addOperation(withdrawal);

        //Then
        Assertions.assertEquals(new BigDecimal(80), account.getBalance());
        Assertions.assertEquals(expectedOperationsList, account.getOperations());
    }

    @Test
    public void withdrawal_should_fail_if_no_enough_money_in_balance() {
        //Given
        final Operation withdrawal = new Operation(new Date(1563366800), new BigDecimal(20), OperationType.WITHDRAWAL);
        final Account account = new Account();
        List<Operation> expectedOperationsList = new ArrayList<>();


        Assertions.assertThrows(NotEnoughMoneyException.class, () -> account.addOperation(withdrawal));
        Assertions.assertEquals(new BigDecimal(0), account.getBalance());
        Assertions.assertEquals(expectedOperationsList, account.getOperations());

    }

    @Test
    public void getHistory_should_list_operation_and_balance() {
        //Given
        Account account= new Account();
        account.addOperation(new Operation(new Date(1563195527000L), new BigDecimal(500), OperationType.DEPOSIT));
        account.addOperation(new Operation(new Date(1563281927000L), new BigDecimal(10), OperationType.DEPOSIT));
        account.addOperation(new Operation(new Date(1563368327000L), new BigDecimal(405), OperationType.WITHDRAWAL));

        //When
        String actualHistory = account.getHistory();

        //Then
        String expectedHistory =
        "2019-07-15 - DEPOSIT - 500\n" +
        "2019-07-16 - DEPOSIT - 10\n" +
        "2019-07-17 - WITHDRAWAL - 405\n" +
        "Balance : 105";
        Assertions.assertEquals(expectedHistory, actualHistory);
    }
}