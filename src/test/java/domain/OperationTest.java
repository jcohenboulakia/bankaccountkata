package domain;

import domain.Operation;
import domain.OperationType;
import domain.exception.IncorrectOperationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

class OperationTest {

    @Test
    public void should_fail_if_amount_is_less_than_or_equal_0() {
        Assertions.assertThrows(IncorrectOperationException.class, ()
                -> new Operation(new Date(1563366800), new BigDecimal(-1), OperationType.DEPOSIT));
        Assertions.assertThrows(IncorrectOperationException.class, ()
                -> new Operation(new Date(1563366800), new BigDecimal(0), OperationType.DEPOSIT));
    }

}