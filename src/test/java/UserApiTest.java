import domain.Account;
import domain.Operation;
import domain.OperationType;
import infra.CurrentDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserApiTest {

    @Mock
    CurrentDate currentDate;

    @InjectMocks
    UserApi userApi;

    Account anAccount;

    @BeforeEach
    void setUp() {
        anAccount = new Account();
    }

    @Test
    public void deposit_and_withdrawal_should_add_operation_with_current_date() {
        //Given
        final int timestampOperation1 = 1563366800;
        final int timestampOperation2 = 1563366900;
        Mockito.when(currentDate.getCurrentDate())
                .thenReturn(new Date(timestampOperation1))
                .thenReturn(new Date(timestampOperation2));

        //When
        userApi.deposit(anAccount, new BigDecimal(100));
        userApi.withdrawal(anAccount, new BigDecimal(20));

        //Then
        List<Operation> expectedOperationsList = new ArrayList<>();

        expectedOperationsList.add(
                new Operation(new Date(timestampOperation1), new BigDecimal(100), OperationType.DEPOSIT));
        expectedOperationsList.add(
                new Operation(new Date(timestampOperation2), new BigDecimal(20), OperationType.WITHDRAWAL));

        Assertions.assertEquals(expectedOperationsList, anAccount.getOperations());
    }

    @Test
    public void history_should_return_history_of_account() {
        //Given
        Account account = Mockito.mock(Account.class);
        Mockito.when(account.getHistory())
                .thenReturn("history");
        //When
        final String history = userApi.history(account);

        //Then
        Assertions.assertEquals("history", history);

    }



}